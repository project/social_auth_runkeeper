<?php

namespace Drupal\social_auth_runkeeper\Settings;

/**
 * Defines an interface for Social Auth Runkeeper settings.
 */
interface RunkeeperAuthSettingsInterface {

  /**
   * Gets the client ID.
   *
   * @return string
   *   The client ID.
   */
  public function getClientId();

  /**
   * Gets the client secret.
   *
   * @return string
   *   The client secret.
   */
  public function getClientSecret();

}
