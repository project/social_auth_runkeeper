<?php

namespace Drupal\social_auth_runkeeper\Controller;

use Drupal\Core\Controller\ControllerBase;
use League\OAuth2\Client\Provider\Runkeeper;
use Drupal\social_api\Plugin\NetworkManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Drupal\social_auth\SocialAuthUserManager;
use Drupal\social_auth_runkeeper\RunkeeperAuthManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Render\RenderContext;

/**
 * Manages requests to the Runkeeper API.
 */
class RunkeeperAuthController extends ControllerBase {


  /**
   * The network plugin manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  protected $networkManager;

  /**
   * The Runkeeper authentication manager.
   *
   * @var \Drupal\social_auth_runkeeper\RunkeeperAuthManager
   */
  protected $runkeeperManager;

  /**
   * The user manager.
   *
   * @var \Drupal\social_auth\SocialAuthUserManager
   */
  protected $userManager;

  /**
   * The session manager.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface
   */
  protected $session;

  /**
   * GoogleLoginController constructor.
   *
   * @param \Drupal\social_api\Plugin\NetworkManager $network_manager
   *   Used to get an instance of social_auth_runkeeper network plugin.
   * @param \Drupal\social_auth\SocialAuthUserManager $userManager
   *   Manages user login/registration.
   * @param \Drupal\social_auth_runkeeper\RunkeeperAuthManager $runkeeperManager
   *   Used to manage authentication methods.
   * @param SessionInterface $session
   *   Used to store the access token into a session variable.
   */
  public function __construct(NetworkManager $network_manager, SocialAuthUserManager $userManager, RunkeeperAuthManager $runkeeperManager, SessionInterface $session) {
    $this->networkManager = $network_manager;
    $this->userManager = $userManager;
    $this->runkeeperManager = $runkeeperManager;
    $this->session = $session;
    // Sets the plugin id.
    $this->userManager->setPluginId('social_auth_runkeeper');
    // Sets the session keys to nullify if user could not logged in.
    $this->userManager->setSessionKeysToNullify(['social_auth_runkeeper_access_token']);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_manager'),
      $container->get('social_auth_runkeeper.manager'),
      $container->get('session')
    );
  }

  /**
   * Redirect to Runkeeper Services Authentication page.
   */
  public function redirectToRunkeeper() {
    // We need to avoid rendering too early.
    $context = new RenderContext();
    /* @var Runkeeper $client */
    $client = \Drupal::service('renderer')->executeInRenderContext($context, function () {
      return $this->networkManager->createInstance('social_auth_runkeeper')->getSdk();
    });

    // If Runkeeper client could not be obtained.
    if (!$client) {
      drupal_set_message($this->t('Social Auth Runkeeper not configured properly. Contact site administrator.'), 'error');
      return $this->redirect('user.login');
    }
    // Runkeeper service was returned, inject it to $linkedinManager.
    $this->runkeeperManager->setClient($client);

    return new TrustedRedirectResponse($client->getAuthorizationUrl(['scope' => ['public']]));
  }

  /**
   * Callback function to login user.
   */
  public function callback() {
    $client = $this->networkManager->createInstance('social_auth_runkeeper')->getSdk();

    $this->runkeeperManager->setClient($client)->authenticate();
    // Saves access token so that event subscribers can call Runkeeper API.
    $this->session->set('social_auth_runkeeper_access_token', $this->runkeeperManager->getAccessToken());

    // Gets user information.
    $user = $this->runkeeperManager->getUserInfo();

    // If user information could be retrieved.
    if ($user) {
      $picture = (isset($user->imageUrl)) ? $user->imageUrl : FALSE;

      $fullname = $user->firstName . ' ' . $user->lastName;
      return $this->userManager->authenticateUser($user->email, $fullname,
        $user->uid, $picture);
    }

    drupal_set_message($this->t('You could not be authenticated, please contact the administrator'), 'error');
    return $this->redirect('user.login');
  }
}
