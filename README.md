Social Auth Runkeeper allows users to register and login to your Drupal site with 
their Runkeeper account. It is based on Social Auth and Social API projects.

This module adds the path user/login/runkeeper which redirects the user to Runkeeper 
for authentication.

After Runkeeper has returned the user to your site, the module compares the email 
address provided by Runkeeper to the site's users. If your site already has an 
account with the same email address, user is logged in. If not, a new user 
account is created.

Login process can be initiated from the "Runkeeper" button in the Social Auth 
block. Alternatively, site builders can place (and theme) a link to 
user/login/runkeeper wherever on the site.